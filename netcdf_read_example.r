#########################################
#
# netcdf_read_example.r    11 March 2016
#
# Reads in the example netCDF file
# produced by netcdf_write_example.c
#
#########################################

# Source the netCDF library
# If you need to install it, uncomment
# the following line:
#install.packages("ncdf4")
library(ncdf4)

# Open the netCDF file
nc <- nc_open("example.nc")

# Calling an open netCDF handle will
# print a summary of the file contents
# which is convenient for finding
# the variable names and dimensions
# (useful if you didn't create the
# file yourself)
nc

# Now you can read in the variables
# from the file using their variable
# names from printed summary
x <- ncvar_get(nc, "x")
y <- ncvar_get(nc, "y")
z <- ncvar_get(nc, "z")

# NOTE: The dimensions of the variables
# are in REVERSE ORDER of how they were
# written.
dim(x)

# Now you can make your plots or do
# your post-processing!

# When you're done with the netCDF file,
# close it.
nc_close(nc)
